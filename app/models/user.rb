class User < ActiveRecord::Base
  attr_accessible :confirmed_at, :email, :full_name, :password, :password_confirmation, :role, :username
end
