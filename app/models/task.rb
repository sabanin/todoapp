class Task < ActiveRecord::Base
  attr_accessible :assignee, :description, :due_date, :owner
end
